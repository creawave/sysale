$( document ).ready(function() {
    const imgBox = [...document.getElementsByClassName('img-box')];

    imgBox.forEach(item => {
        const img = item.firstElementChild.getAttribute('src');
        const hover = item.firstElementChild.getAttribute('data-hover');
        item.onmouseenter = function() {
            this.firstElementChild.src = hover;
        };

        item.onmouseleave = function() {
            this.firstElementChild.src = img;
        }
    });

    const icons = [...document.getElementsByClassName('icon-img')];

    icons.forEach(item => {
        const img = item.getAttribute('data-icon');
        const checked = item.getAttribute('data-checked');
        item.addEventListener("click", function() {
            if(this.getAttribute('src') === img) {
                this.src = checked;
            } else {
                this.src = img;
            }
        });
    });

    const droplist = [...document.getElementsByClassName('main-droplist')];

    droplist.forEach(item => {
        const ul = item.querySelector('.droplist');
        const value = item.querySelector('.droplist-value');
        const list = [...item.querySelectorAll('.droplist-item')];
        const img = item.querySelector('.droplist-img');
        value.addEventListener("click", function() {
            ul.classList.toggle('open');
            img.classList.toggle('open');
        });
        list.forEach(elem => {
            elem.addEventListener("click", function() {
                const option = elem.getAttribute('data-value');
                value.innerHTML = option;
                list.forEach(item => {
                    img.classList.remove('open');
                    ul.classList.remove('open');
                    if(item.getAttribute('data-value') === option) {
                        item.classList.add('active');
                    } else {
                        item.classList.remove('active');
                    }
                })
            });
        })
    });

    const block = [...document.getElementsByClassName('main-box')];

    block.forEach(item => {
        const ul = item.querySelector('.droplist');
        item.onmouseleave = function () {
            ul.classList.remove('open')
        }
    });

    const calculation = [...document.getElementsByClassName('main-calculation')];

    calculation.forEach(item => {
        const state = {
            price: Number(item.querySelector('.price').innerHTML),
            quantity: Number(item.querySelector('.buttonCountNumber').innerHTML),
            setQuantity: function (value) {
                this.quantity = value
            },
            setPrice: function (value) {
                this.price = value
            },
            getFullPrice: function () {
                return this.quantity * this.price
            }
        };
        const buttonCountPlus = [...item.getElementsByClassName("buttonCountPlus")];
        const buttonCountMinus = [...item.getElementsByClassName("buttonCountMinus")];
        const price = item.querySelector('.price');
        const volume = [...item.querySelectorAll('.volume')];

        volume.forEach(item => {
            item.addEventListener('input', function () {
                state.setPrice(Number(item.getAttribute('data-price')));
                price.innerHTML = state.getFullPrice();
            })
        });

        buttonCountPlus.forEach((item) => {
            item.addEventListener("click", function() {
                this.closest('div').childNodes[3].innerHTML++;
                state.setQuantity(state.quantity+1);
                price.innerHTML = state.getFullPrice();
            });
        });

        buttonCountMinus.forEach((item) => {
            item.addEventListener("click", function() {
                if(this.closest('div').childNodes[3].innerHTML >= 2) {
                    this.closest('div').childNodes[3].innerHTML--;
                    state.setQuantity(state.quantity-1);
                    price.innerHTML = state.getFullPrice();
                }
            });
        });
    })
});
